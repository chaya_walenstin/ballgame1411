﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private Transform myTtansform;
    public float Ymove = -1f;
    public float acceleration = 1;
    public Transform ball;
    public Camera mycamera;
    // Use this for initialization
    void Start ()
    {
        myTtansform = GetComponent<Transform>();
    }


    private void Update()
    {
       // myTtansform.Translate(0, Ymove*Time.deltaTime, 0);
      //  Ymove = Time.deltaTime - speed;

        if (ball.position.y < myTtansform.position.y - mycamera.orthographicSize)
        {
            myTtansform.position = new Vector3(myTtansform.position.x, ball.position.y + mycamera.orthographicSize,myTtansform.position.z);
        }
        else
        {
            myTtansform.Translate(0, Ymove * Time.deltaTime, 0);
            Ymove -= Time.deltaTime * acceleration;
        }
    }
}
