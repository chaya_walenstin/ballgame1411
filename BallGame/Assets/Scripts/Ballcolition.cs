﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Ballcolition : MonoBehaviour
{
    public Text coinText;

    private AudioSource MyAodioSource;
    public AudioClip cointSFX;
    public AudioClip winSFX;
    public AudioClip gameOverSFX;
    public AudioSource gameMusicAudioSource;
    public AudioClip failureSFX;

    public List<Sprite> lifeSprite;
    public Sprite fullLifeSprite;
    public Sprite middleLifeSprite;
    public Sprite littleLifeSprite;
    public Image lifeImage;
    public Transform cameraTransform;

    public GameObject canvasGameOver;
    public GameObject canvasGood;

    public CameraMove cameraMove;

    private List<GameObject> redSticksTouched;

    private void Start()
    {
        MyAodioSource = GetComponent<AudioSource>();

        coinText.GetComponent<UserCoin>().coins=0;
        coinText.text = coinText.GetComponent<UserCoin>().coins.ToString();

        lifeSprite = new List<Sprite>() { littleLifeSprite, middleLifeSprite , fullLifeSprite};
        lifeImage.GetComponent<UserLife>().life = 2;
        lifeImage.sprite = lifeSprite[lifeImage.GetComponent<UserLife>().life];
        redSticksTouched = new List<GameObject>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("RedStick"))
        {
            foreach(GameObject redGo in redSticksTouched)
            {
                if(collision.gameObject == redGo)
                {
                    return;
                }
            }

            if (lifeImage.GetComponent<UserLife>().life > 0)
            {
                lifeImage.GetComponent<UserLife>().life--;
                MyAodioSource.PlayOneShot(failureSFX);
                lifeImage.sprite = lifeSprite[lifeImage.GetComponent<UserLife>().life];
                redSticksTouched.Add(collision.gameObject);
                Handheld.Vibrate();
            }
            else
            {
                canvasGameOver.SetActive(true);
                Time.timeScale = 0;  // STOP GAME
                MyAodioSource.PlayOneShot(gameOverSFX);
                gameMusicAudioSource.Stop();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.gameObject.CompareTag("coin"))
        {
            Destroy(info.gameObject);
            coinText.GetComponent<UserCoin>().coins++;
            coinText.text = coinText.GetComponent<UserCoin>().coins.ToString();
            MyAodioSource.PlayOneShot(cointSFX);
        }
       

        else if (info.gameObject.CompareTag("WinLevel"))
        {
            canvasGood.SetActive(true);
            //Time.timeScale = 0;  // STOP
            MyAodioSource.PlayOneShot(winSFX);
            gameMusicAudioSource.Stop();

            cameraMove.enabled = false;
            gameObject.GetComponent<BallMove>().enabled = false;
            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

            PlayerPrefs.SetInt("myCoints", coinText.GetComponent<UserCoin>().coins);
            Invoke("GoToResult", winSFX.length-3);
        }
        else
        {
            if (lifeImage.GetComponent<UserLife>().life > 0)
            {
                lifeImage.GetComponent<UserLife>().life--;
                MyAodioSource.PlayOneShot(failureSFX); 
                lifeImage.sprite = lifeSprite[lifeImage.GetComponent<UserLife>().life];
                cameraTransform.transform.Translate(0, 5, 0);
            }
            else
            {
             canvasGameOver.SetActive(true);
             Time.timeScale = 0;  // STOP GAME
             MyAodioSource.PlayOneShot(gameOverSFX);
             gameMusicAudioSource.Stop();
            }

           
        }
    }

    private void GoToResult()
    {
        print("move to finish");
        SceneManager.LoadScene(2);
    }


}
