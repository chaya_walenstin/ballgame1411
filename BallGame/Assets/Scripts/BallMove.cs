﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    private Rigidbody2D ball;
    public float speed = 4;

	// Use this for initialization
	void Start ()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        ball = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                ball.AddForce(Vector2.right * speed);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                ball.AddForce(Vector2.left * speed);
            }
        }
        else
        {
            if(Input.acceleration.x > 0.3)
            {
                ball.AddForce(Vector2.right * speed);
            }
            if (Input.acceleration.x < -0.3)
            {
                ball.AddForce(Vector2.left * speed);
            }

        }

    }
}
