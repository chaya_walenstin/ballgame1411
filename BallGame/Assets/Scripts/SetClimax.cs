﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetClimax : MonoBehaviour {

    public Text yourCointsNumberText;
    public Text lastClimaxText;

    // Use this for initialization
    void Start () {
        var lastMaxCoints = PlayerPrefs.GetInt("climax");
        var myCointsNumber = PlayerPrefs.GetInt("myCoints");

        if (myCointsNumber > lastMaxCoints)
        {
            PlayerPrefs.SetInt("climax", myCointsNumber);
        }

        yourCointsNumberText.text = "You get " + myCointsNumber + " coins!!!";
        lastClimaxText.text = "The climax is " + lastMaxCoints;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
