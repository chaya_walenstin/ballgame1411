﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBorder : MonoBehaviour
{
    private Transform MyTransform;
    public SpriteRenderer SpriteRandererRight;
    public SpriteRenderer SpriteRandererLeft;
    public Transform BordreLeft;
    public Transform BordreRight;
    public Camera MyCamera;

    public Transform MyBackground;

    // Use this for initialization
    void Start ()
    {
        MyTransform = GetComponent<Transform>();

        float HalfSizeR;  // הוספתי 
        float HalfSizeL;  // הוספתי 
        HalfSizeR = SpriteRandererRight.bounds.size.x / 2;  // הוספתי 16
        HalfSizeL = SpriteRandererLeft.bounds.size.x / 2;  // הוספתי 27

        float Ratio = Screen.width / (float)Screen.height;
        float CameraHight = MyCamera.orthographicSize * 2;
        float CameraWhith = CameraHight * Ratio;
        float HalfeCameraWithe = CameraWhith / 2f;

        BordreLeft.position = MyCamera.transform.position - new Vector3(HalfeCameraWithe, 0, 0);
        BordreRight.position = MyCamera.transform.position + new Vector3(HalfeCameraWithe, 0, 0);

        // BordreLeft.Translate(SpriteRandererLeft.bounds.size.x, 0, 1); //  מקור
        // BordreRight.Translate(-SpriteRandererLeft.bounds.size.x, 0, 1); // מקור

        BordreLeft.Translate(HalfSizeL, 0, 1);  //הוספה
        BordreRight.Translate(-HalfSizeR, 0, 1);  //הוספה

    }
	
	// Update is called once per frame
	void Update ()
    {
        MyBackground.position = new Vector3(MyCamera.transform.position.x, MyCamera.transform.position.y, MyBackground.position.z);

        Vector3 newPosition = MyTransform.position;
        newPosition.x = Mathf.Clamp(newPosition.x, BordreLeft.position.x, BordreRight.position.x);
        MyTransform.position = newPosition;

        BordreLeft.position = new Vector3(BordreLeft.position.x , MyCamera.transform.position.y, BordreLeft.position.z);
        BordreRight.position = new Vector3(BordreRight.position.x, MyCamera.transform.position.y, BordreRight.position.z);

    }
}
